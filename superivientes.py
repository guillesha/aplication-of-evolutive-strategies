#archivo que explica la seleccion de supervivientes
#método que aplica ls seleccion de superfivientes u,lambda
import heapq as heap
import individuo as ind
import poblacion as pop
import numpy as np


#funcion calcular los u mejores de una poblacion dada powell
def uMejores(poblacion,u,nVariables,vStepSize,lb,hb):
    mejores=pop.Poblacion(0,nVariables,vStepSize,lb,hb)
    h=[]
    for i in range(0,poblacion.nIndividuos):
        heap.heappush(h,(poblacion.poblacion[i].getFitness(),poblacion.poblacion[i]))
    extraccion =[heap.heappop(h) for i in range(u)]
    for x in extraccion:
        mejores.anadirIndividuo(x[1])
    return mejores
#funcion que devuelve el mejor individuo encontrado
def mejor(poblacion):
    h = []
    for i in range(0, poblacion.nIndividuos):
        heap.heappush(h, (poblacion.poblacion[i].getFitness(), poblacion.poblacion[i]))
    mejor = heap.heappop(h)
    return mejor
#funcion mejores para la ecuacion xin
#funcion calcular los u mejores de una poblacion dada
#funcion calcular los u mejores de una poblacion dada
def uMejoresXin(poblacion,u,nVariables,vStepSize,lb,hb):
    mejores=pop.Poblacion(0,nVariables,vStepSize,lb,hb)
    h=[]
    valores=[]
    for n in range(len(poblacion.poblacion)):
        listaINdividuos=poblacion.poblacion[:]
        individuo=listaINdividuos[n]

        heap.heappush(h,individuo)
    extraccion =[heap.heappop(h) for i in range(u)]

    for x in extraccion:
        mejores.anadirIndividuo(x)
    return mejores



def mejorXin(poblacion):
    h=[]
    for n in range(len(poblacion.poblacion)):
        listaINdividuos=poblacion.poblacion[:]
        individuo=listaINdividuos[n]

        heap.heappush(h,(individuo))
    mejor = heap.heappop(h)
    return mejor

def uMejoresXin2(poblacion):
    mejores=[]
    h=[]

    for n in range(len(poblacion)):
        listaINdividuos=poblacion[:]
        individuo=listaINdividuos[n]
        u=2
        heap.heappush(h,[abs(poblacion[n]+1),poblacion[n]])
    extraccion =[heap.heappop(h) for i in range(u)]

    for x in extraccion:
        mejores.append(x[1])
    return mejores

