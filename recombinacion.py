#archivo que define las funciones de recombinación
import poblacion as pop
import individuo as ind
import random as rand
import numpy as np
def creacionHijo(poblacion):
    hijo=ind.Individuo(poblacion.nVariables,poblacion.stepSize,poblacion.lb,poblacion.hb)
    #seleccionamos dos padres aleatorios para cada xi del hijo parametros objeto
    for i in range(0,poblacion.nVariables):
        padre1=poblacion.poblacion[rand.randint(0,poblacion.nIndividuos-1)]
        padre2=poblacion.poblacion[rand.randint(0,poblacion.nIndividuos-1)]
        #seleccionamos un allel aleatorio de un padre o del otro(recombinacion discreta)
        probabilidad=rand.uniform(0,1)
        if probabilidad>0.5:
            hijo.representation[i]=padre1.representation[rand.randint(0,poblacion.nVariables-1)]
        else:
            hijo.representation[i] = padre2.representation[rand.randint(0, poblacion.nVariables-1)]
    #ahora aplicamos la recombinacion intermediaria para los párametros estratégicos
    for k in range(0,poblacion.stepSize):
        padre1 = poblacion.poblacion[rand.randint(0, poblacion.nIndividuos-1)]
        padre2 = poblacion.poblacion[rand.randint(0, poblacion.nIndividuos-1)]
        hijo.stepsize.values[k]=np.mean([padre1.stepsize.values[k],padre2.stepsize.values[k]])
    return hijo
def crearOffSpring(padres,lamda,nVariables,vStepSize,lb,hb):
    offSpring= pop.Poblacion(0,nVariables,vStepSize,lb,hb)
    for i in range(lamda):
        offSpring.anadirIndividuo(creacionHijo(padres))

    return offSpring
