import individuo as ind
import numpy as np
import math as math
import random as rand
#archivo define las mutaciones
#devuelve el individuo mutado
def mutarIndividuoOneStep(tao,individuo,lb,hb,taoprima=0):
    representacio= individuo.representation
    stepsize=individuo.stepsize.values[0]
    #mutamos stepsize gamma'=gamma * e**(tao*N(01))
    if taoprima==0:
        #parte mutacion one stepsize
        stepsize = individuo.stepsize.values[0]
        valor=mutatationOneStepSize(stepsize,tao)

        individuo.stepsize.values[0]=valor

        for i in range(0, len(representacio)):
            # aplicamos la formula 4.3 del libro xi'=xi+g'*N(0,1)

            representacio[i] = representacio[i] + individuo.stepsize.values[0] * incrementoGaussiano(1)
            if representacio[i]<lb:
                estigma=lb-lb/9
                representacio[i]=rand.uniform(lb,estigma)
            elif representacio[i]>hb:
                estigma = hb - hb / 9
                representacio[i] = rand.uniform(hb,estigma)

    else:
        #parte de implmentación n step sizes
        for n in range(len(individuo.stepsize.values)):
            stepsize = individuo.stepsize.values[n]
            individuo.stepsize.values[n]=mutatuinNStepSize(stepsize,tao,taoprima)
        for s in range(0,len(representacio)):

            representacio[s]=representacio[s] + individuo.stepsize.values[s] * incrementoGaussiano(1)
            if representacio[s] < lb:
                estigma = lb - lb / 9
                representacio[s] = rand.uniform(lb, estigma)
            elif representacio[s] > hb:
                estigma = hb - hb / 9
                representacio[s] = rand.uniform(estigma,hb)


    return individuo



#fucnion que define el incremento gaussiano
def incrementoGaussiano(gamma):
    return np.random.normal(0,gamma)
#furmala para caculcular la derivada de la desviación el one step size
def mutatationOneStepSize(stepSize,tao):
    valor=stepSize*(math.e**(tao*incrementoGaussiano(1)))

    return valor

#fórmula para calcular la derivada en n stepsizes
def mutatuinNStepSize(stepSize,tao,taoprima):
    return stepSize*math.e**((taoprima*incrementoGaussiano(1))+(tao*incrementoGaussiano(1)))
