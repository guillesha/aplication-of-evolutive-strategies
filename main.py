import matplotlib.pyplot as plt
import poblacion as pop
import recombinacion as rec
import superivientes as sup
import math as math
import matplotlib as mpl
import matplotlib.pyplot as plt
from pyexcel_ods3 import save_data
from collections import OrderedDict
#crear poblacion de 200 individuos
lamda=200
muval=30
nVariables=5
vStepSize=1
taoprima=1 / 2*math.sqrt(nVariables)
tao=1 / math.sqrt(2*math.sqrt(nVariables))
lb=-1
hb=1
poblacionInicial=pop.Poblacion(muval,nVariables,vStepSize,lb,hb)
#variables apra pruebas
aes=0
mejor=0.00
sr=0
from pyexcel_ods3 import save_data
iteracionesList=[]
fitness=[]

def iterativa(poblacionInicial,iteraciones,mulamda):
    aes=0
    for i in range(iteraciones):
        #valor de garantía se utiliza un errore de 0,00001 para dar como  válida la solución
        if  sup.mejor(poblacionInicial)[1].getFitness()==0:
            print('Solución encontrada')
            break
        else:
            # crear 200 hijos de esos mejores 20
            hijos = rec.crearOffSpring(poblacionInicial, lamda, nVariables, vStepSize, lb, hb)
            # mutamos los hijos onestep
            hijos.mutacionPoblacionOneStep(tao,lb,hb)

            #hijos.mutacionPoblacionNstep(tao,taoprima,lb,hb)
            #consideraciones mu + lamada
            if mulamda:
              padresmashijos = pop.Poblacion(0, nVariables, vStepSize, lb, hb)
              padresmashijos.anadirIndividuos(hijos)
              padresmashijos.anadirIndividuos(poblacionInicial)
              mu = sup.uMejores(padresmashijos, 30, nVariables, vStepSize, lb, hb)
            else:
                #u,lamda
                mu = sup.uMejores(hijos, 30, nVariables, vStepSize, lb, hb)
            poblacionInicial=mu
            print(sup.mejor(poblacionInicial)[1].getFitness())
            print(i)
            iteracionesList.append(i)
            fitness.append(sup.mejor(poblacionInicial)[1].getFitness())
            if  sup.mejor(poblacionInicial)[1].getFitness()> 1e-15:
                aes=aes+1
                print('hola')
    return sup.mejor(poblacionInicial)[1].getFitness()
    print(sup.mejor(poblacionInicial)[1].getFitness())
    print(str(aes)+'numero de generaciones al éxito')

iterativa(poblacionInicial,1000,False)
#representación
#
# plt.plot(iteracionesList, fitness, 'ro')
# plt.xlabel('Iteraciones')
# plt.ylabel('Fitness')
# plt.axis([0,len(iteracionesList),0,0.0000000000001])
# plt.title('Gráfica evolución del AE')
# plt.show()
#to ods tests
# listaMBF=[]
# listaAES=[]
# for i in range(1):
#     listaMBF.append(float(iterativa(poblacionInicial,1000,False)))
#     listaAES.append(aes)
#
# print(listaMBF)
# data = OrderedDict() # from collections import OrderedDict
# data.update({"Sheet 1": [listaMBF[:], [4, 5, 6]]})
# data.update({"Sheet 2": [["row 1", "row 2", "row 3"]]})
# save_data("resultados.ods", data)
