import individuo as ind
import mutacion as mut
import superivientes as sup
import random as rand

class Poblacion():

    def __init__(self,nIndividuos,nVariables,vStepSize,lb,hb):
        poblacion=[]
        if nIndividuos>0:
            for i in range(0,nIndividuos):
                 poblacion.append(ind.Individuo(nVariables,vStepSize,lb,hb))
        self.poblacion=poblacion
        self.nIndividuos=nIndividuos
        self.nVariables=nVariables
        self.stepSize=vStepSize
        self.lb=lb
        self.hb=hb

    def mutacionPoblacionOneStep(self,tao,lb,hb):
        value= sup.mejorXin(self).stepsize.values[0]
        for individuo in self.poblacion:
                mut.mutarIndividuoOneStep(tao,individuo,lb,hb)
    def mutacionPoblacionNstep(self,tao,taoprima,lb,hb):
        for individuo in self.poblacion:
            mut.mutarIndividuoOneStep(tao,individuo,lb,hb,taoprima)


    def anadirIndividuo(self,individuo):
        self.nIndividuos=self.nIndividuos+1
        self.poblacion.append(individuo)


    def anadirIndividuos(self,poblacion):
        self.nIndividuos=self.nIndividuos+poblacion.nIndividuos

        self.poblacion=self.poblacion+poblacion.poblacion



    def definirSigmas(self):
        prop=rand.uniform(1,2)
        for i in self.poblacion:
            i.stepsize.values[0]=prop


