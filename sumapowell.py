import matplotlib.pyplot as plt

#fichero de representación de la función suma de powell
#calcula el valor fitness de la funcion powell
def funcionPowell(individuo,i=0,resultado=0):
   if i==len(individuo):
       return resultado
   else:
       resultado=resultado+(abs(individuo[i]))**(i+2)
       return funcionPowell(individuo,i+1,resultado)



