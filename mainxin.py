import poblacion as pop
import recombinacion as rec
import superivientes as sup
import math as math
from pyexcel_ods3 import save_data
from collections import OrderedDict
#crear poblacion de 200 individuos
lamda=200
muval=30
nVariables=5
vStepSize=5
taoprima=(1 / 2*math.sqrt(nVariables))/50
tao=(1 / math.sqrt(2*math.sqrt(nVariables)))/50
lb=-10
hb=10
poblacionInicial=pop.Poblacion(muval,nVariables,vStepSize,lb,hb)
sigmas=[]
fitness=[]

def iterativa(poblacionInicial,iteraciones,mulamda):
    iter=0
    n=0
    aes=0
    sr=0
    for i in range(iteraciones):
        #valor de garantía se utiliza un errore de 0,00001 para dar como  válida la solución
        if  sup.mejorXin(poblacionInicial).getFitnessXin()==-1:
            print('Solución encontrada')
            break
        else:

            # crear 200 hijos de esos mejores 20
            hijos = rec.crearOffSpring(poblacionInicial, lamda, nVariables, vStepSize, lb, hb)
            # mutamos los hijos onestep
            #hijos.mutacionPoblacionOneStep(tao,lb,hb)
            #aplcación de elitismo
            mejorAnterior=sup.mejorXin(poblacionInicial)
            nuevo=sup.mejorXin(hijos)
            hijos.anadirIndividuo(mejorAnterior)



            hijos.mutacionPoblacionNstep(tao,taoprima,lb,hb)
            #consideraciones mu + lamda
            if mulamda:
              padresmashijos = pop.Poblacion(0, nVariables, vStepSize, lb, hb)
              padresmashijos.anadirIndividuos(hijos)
              padresmashijos.anadirIndividuos(poblacionInicial)

              mu = sup.uMejores(padresmashijos, 30, nVariables, vStepSize, lb, hb)
            else:
                #u,lamda
                mu = sup.uMejoresXin(hijos, 5, nVariables, vStepSize, lb, hb)

            if  n==500:
                mu.definirSigmas()
            if  sup.mejor(poblacionInicial)[1].getFitness()>-0.75:
                    aes=aes+1
            else:
                    sr=1
            poblacionInicial=mu
            ultimo=sup.mejorXin(mu)
            ultimo.imprimirIndividuoFitStep()
            print(sup.mejorXin(poblacionInicial).getFitnessXin())
            sigmas.append(ultimo.getFitnessXin())
    return sup.mejorXin(poblacionInicial).getFitnessXin(),aes,sr
iterativa(poblacionInicial,1000,True)
#
# #to ods tests
# listaSR=[]
# listaMBF=[]
# listaAES=[]
# for i in range(20):
#     MBF,AES,sr=iterativa(poblacionInicial,1000,True)
#     listaMBF.append(float(MBF))
#     listaAES.append(AES)
#     listaSR.append(sr)
#
# print(listaMBF)
# data = OrderedDict() # from collections import OrderedDict
# data.update({"Sheet 1": [listaSR[:],listaMBF[:], listaAES[:]]})
# save_data("resultadosxin4.ods", data)
