#clase que define cada uno de los individuos
import sumapowell as sp
import random as rand
import stepsize as st
import xinsheyang as xin
class Individuo(object):

    def __init__(self,n,vStepSize,lb,hb):
        self.representation=[]
        for i in range(0, n):
            self.representation.append(rand.uniform(lb, hb))
        self.fitness=0
        self.fitnessXin=0
        self.stepsize=st.Stepsize(vStepSize)


    #devuelve el fitness segun la funcón dada
    def calcularFitnessPowell(self):
        self.fitness=sp.funcionPowell(self.representation)

    def getFitness(self):
        if self.fitness !=0:
            return self.fitness
        else:
            valor=sp.funcionPowell(self.representation)
            self.fitness= valor
            return valor
    #imprime al individuo
    def imprimirIndividuo(self):
        print(self.representation)
        #imprime el stepsize
        #imprime al individuo
    def imprimirIndividuoFitStep(self):
        print(str(self.getFitnessXin())+str(self.stepsize.values))
    def imprimirStepSize(self):
        print(self.stepsize.values)
        #devuelve als varaibles gamma
    def getStepSize(self):
        return self.stepsize
    def getFitnessXin(self):
        if self.fitnessXin !=0:
            return self.fitnessXin
        else:
            valor=xin.xinsheyang(self.representation)
            self.fitnessXin=valor
            return valor


    def  __lt__(self,other):
        return abs(self.getFitnessXin()+1)<abs(other.getFitnessXin()+1)

